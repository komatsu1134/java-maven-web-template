package com.xskj.mapper;

import com.xskj.pojo.Demo;
import com.xskj.utils.SqlSessionServletFactory;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.Test;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author : xskj
 * @description: demo测试类
 * @date : 2021/11/30 18:35
 */

public class DemoMapperTest {

    /*
     * test method for query of Demo
     */
    @Test
    public void testSelectAll() throws IOException {
        //1.获取连接对象
        SqlSessionFactory sqlSessionFactory = SqlSessionServletFactory.getSqlSessionFactory();
        //2.获取sqlSession对象
        SqlSession sqlSession = sqlSessionFactory.openSession();

        //3. 获取实体对象Demo的Mapper
        DemoMapper demoMapper = sqlSession.getMapper(DemoMapper.class);
        //4. 执行操作方法
        List<Demo> demo = demoMapper.selectAll();
        System.out.println(demo);


        //5.释放对象内存
        sqlSession.close();
    }

    @Test
    public void testSelectById() throws IOException {
        //1.获取连接对象
        SqlSessionFactory sqlSessionFactory = SqlSessionServletFactory.getSqlSessionFactory();
        //2.获取sqlSession对象
        SqlSession sqlSession = sqlSessionFactory.openSession();


        //3. 获取实体对象Demo的Mapper
        DemoMapper demoMapper = sqlSession.getMapper(DemoMapper.class);
        //4. 执行操作方法
        Demo demo = demoMapper.selectById(24);
        System.out.println(demo);


        //5.释放对象内存
        sqlSession.close();
    }

    @Test
    public void testSelectCondition() throws IOException {
        //1.获取连接对象
        SqlSessionFactory sqlSessionFactory = SqlSessionServletFactory.getSqlSessionFactory();
        //2.获取sqlSession对象
        SqlSession sqlSession = sqlSessionFactory.openSession();

        //3. 获取实体对象Demo的Mapper
        DemoMapper demoMapper = sqlSession.getMapper(DemoMapper.class);

        //4. gain data
        int status = 1;
        String email = "1";
        String name = "xskj";
        //process data
        email = "%" + email + "%";
        name = "%" + name + "%";

        //方法2
      /*Demo demo2 = new Demo();
        demo2.setStatus(status);
        demo2.setName(name);
        demo2.setEmail(email);
        List<Demo> demo = demoMapper.selectByCondition(demo2);*/

        //方法1
        //4. gain data
        // List<Demo> demo = demoMapper.selectByCondition(status, email, name);

        //方法3
        Map map = new HashMap();
        map.put("status", status);
        map.put("email", email);
        map.put("name", name);
        List<Demo> demo = demoMapper.selectByCondition(map);


        System.out.println(demo);
        sqlSession.close();
    }

    @Test
    public void testSelectDynamicCondition() throws IOException {
        //1.获取连接对象
        SqlSessionFactory sqlSessionFactory = SqlSessionServletFactory.getSqlSessionFactory();
        //2.获取sqlSession对象
        SqlSession sqlSession = sqlSessionFactory.openSession();

        //3. 获取实体对象Demo的Mapper
        DemoMapper demoMapper = sqlSession.getMapper(DemoMapper.class);


        //4. gain data
        int status = 1;
        String email = "";
        String name = "";
        //process data
        email = "%" + email + "%";
        name = "%" + name + "%";

        //方法2
   /*   Demo demo2 = new Demo();
        demo2.setStatus(status);
        demo2.setName(name);
        demo2.setEmail(email);
        List<Demo> demo = demoMapper.selectByCondition(demo2);*/

        //方法1
        //4. gain data
        // List<Demo> demo = demoMapper.selectByCondition(status, email, name);

        //方法3
        Map map = new HashMap();
        map.put("status", status);
        map.put("email", email);
        map.put("name", name);
        List<Demo> demo = demoMapper.selectDynamicByCondition(map);


        System.out.println(demo);
        System.out.println(demo.size());
        sqlSession.close();
    }


    @Test
    public void testSelectSingleDynamicByCondition() throws IOException {
        //1.获取连接对象
        SqlSessionFactory sqlSessionFactory = SqlSessionServletFactory.getSqlSessionFactory();
        //2.获取sqlSession对象
        SqlSession sqlSession = sqlSessionFactory.openSession();

        //3. 获取实体对象Demo的Mapper
        DemoMapper demoMapper = sqlSession.getMapper(DemoMapper.class);


        //4. gain data
        int status = 1;
        String email = "";
        String name = "";
        //process data
        email = "%" + email + "%";
        name = "%" + name + "%";

        //方法2
   /*   Demo demo2 = new Demo();
        demo2.setStatus(status);
        demo2.setName(name);
        demo2.setEmail(email);
        List<Demo> demo = demoMapper.selectByCondition(demo2);*/

        //方法1
        //4. gain data
        // List<Demo> demo = demoMapper.selectByCondition(status, email, name);

        //方法3
        Map map = new HashMap();
        map.put("status", status);
        map.put("email", email);
        map.put("name", name);
        List<Demo> demo = demoMapper.selectSingleDynamicByCondition(map);


        System.out.println(demo);
        System.out.println(demo.size());
        sqlSession.close();
    }

    @Test
    public void testAdd() throws IOException {
        //1.获取连接对象
        SqlSessionFactory sqlSessionFactory = SqlSessionServletFactory.getSqlSessionFactory();
        //2.获取sqlSession对象
        SqlSession sqlSession = sqlSessionFactory.openSession(true);


        //3. 获取实体对象Demo的Mapper
        DemoMapper demoMapper = sqlSession.getMapper(DemoMapper.class);
        //4. gain data

        String name = "123";
        String url = "www.xskj.cn";
        String description = "test";
        Date time = new Date();
        String level = "importtance";
        String email = "1213134134@qq.com";
        Integer status = 1;

        //方法2
        Demo demo2 = new Demo();
        demo2.setStatus(status);
        demo2.setName(name);
        demo2.setEmail(email);
        demo2.setUrl(url);
        demo2.setTime(time);
        demo2.setDescription(description);
        demo2.setLevel(level);
        try{
            int i = demoMapper.add(demo2);
            //获取添加的主键值id
            System.out.println(demo2.getId());
            System.out.println("add successful!");
        }catch (Exception e){
            System.out.println(e);
        }


        //方法1
        //4. gain data
        // List<Demo> demo = demoMapper.selectByCondition(status, email, name);

        //方法3
       /* Map map = new HashMap();
        map.put("status", status);
        map.put("email", email);
        map.put("name", name);
        List<Demo> demo = demoMapper.selectSingleDynamicByCondition(map);
       */

        //commit business
        sqlSession.commit();

        sqlSession.close();
    }

    @Test
    public void testUpdate() throws IOException {
        //1.获取连接对象
        SqlSessionFactory sqlSessionFactory = SqlSessionServletFactory.getSqlSessionFactory();
        //2.获取sqlSession对象
        SqlSession sqlSession = sqlSessionFactory.openSession();

        //3. 获取实体对象Demo的Mapper
        DemoMapper demoMapper = sqlSession.getMapper(DemoMapper.class);


        //4. gain data

        String name = "66666";
        String url = "www.xskj.cnn";
        String description = "test1";
        String level = "importtant";
        String email = "12131413@qq.com";
        Integer status = 0;
        Integer id = 1;

        //方法2
        Demo demo2 = new Demo();
        demo2.setStatus(status);
        demo2.setName(name);
        demo2.setEmail(email);
        demo2.setUrl(url);
        demo2.setDescription(description);
        demo2.setLevel(level);
        demo2.setId(id);
        try{
            int i = demoMapper.update(demo2);
            //获取添加的主键值id
            System.out.println(i);
            System.out.println(demo2.getId());
            System.out.println("update successful!");
        }catch (Exception e){
            System.out.println(e);
        }

        //方法1
        //4. gain data
        // List<Demo> demo = demoMapper.selectByCondition(status, email, name);

        //方法3
       /* Map map = new HashMap();
        map.put("status", status);
        map.put("email", email);
        map.put("name", name);
        List<Demo> demo = demoMapper.selectSingleDynamicByCondition(map);
       */
        //commit business
        sqlSession.commit();
        sqlSession.close();
    }


    @Test
    public void testDynamicUpdate() throws IOException {
        //1.获取连接对象
        SqlSessionFactory sqlSessionFactory = SqlSessionServletFactory.getSqlSessionFactory();
        //2.获取sqlSession对象
        SqlSession sqlSession = sqlSessionFactory.openSession();


        //3. 获取实体对象Demo的Mapper
        DemoMapper demoMapper = sqlSession.getMapper(DemoMapper.class);
        //4. gain data

        String name = "name";
        String url = "www.xskj.cnn";
        String description = "1213213";
        String level = "importance";
        String email = "12131413@qq.com";
        Integer id = 1;

        //方法2
        Demo demo2 = new Demo();
        demo2.setName(name);
        demo2.setEmail(email);
        demo2.setUrl(url);
        demo2.setDescription(description);
        demo2.setLevel(level);
        demo2.setId(id);
        try{
            int i = demoMapper.dynamicUpdate(demo2);
            //获取添加的主键值id
            System.out.println(i);
            System.out.println(demo2.getId());
            System.out.println("dynamic update successful!");
        }catch (Exception e){
            System.out.println(e);
        }

        //方法1
        //4. gain data
        // List<Demo> demo = demoMapper.selectByCondition(status, email, name);

        //方法3
       /* Map map = new HashMap();
        map.put("status", status);
        map.put("email", email);
        map.put("name", name);
        List<Demo> demo = demoMapper.selectSingleDynamicByCondition(map);
       */
        //commit business
        sqlSession.commit();
        sqlSession.close();
    }

    @Test
    public void testDelete() throws IOException {
        //1.获取连接对象
        SqlSessionFactory sqlSessionFactory = SqlSessionServletFactory.getSqlSessionFactory();
        //2.获取sqlSession对象
        SqlSession sqlSession = sqlSessionFactory.openSession();


        //3. 获取实体对象Demo的Mapper
        DemoMapper demoMapper = sqlSession.getMapper(DemoMapper.class);
        //4. gain data

        Integer id = 15;

        //方法2
        Demo demo2 = new Demo();
        demo2.setId(id);
        try{
            int i = demoMapper.delete(demo2);
            //获取添加的主键值id
            System.out.println(i);
            System.out.println("delete successful!");
        }catch (Exception e){
            System.out.println(e);
        }

        //方法1
        //4. gain data
        // List<Demo> demo = demoMapper.selectByCondition(status, email, name);

        //方法3
       /* Map map = new HashMap();
        map.put("status", status);
        map.put("email", email);
        map.put("name", name);
        List<Demo> demo = demoMapper.selectSingleDynamicByCondition(map);
       */
        //commit business
        sqlSession.commit();
        sqlSession.close();
    }

    @Test
    public void testDeleteMultiple() throws IOException {
        //1.获取连接对象
        SqlSessionFactory sqlSessionFactory = SqlSessionServletFactory.getSqlSessionFactory();
        //2.获取sqlSession对象
        SqlSession sqlSession = sqlSessionFactory.openSession();


        //3. 获取实体对象Demo的Mapper
        DemoMapper demoMapper = sqlSession.getMapper(DemoMapper.class);
        //4. gain data

        int[] ids = {16, 17, 18, 19, 20};
        //方法2
        Demo demo2 = new Demo();
        try{
            int i = demoMapper.deleteMultiple(ids);
            //获取添加的主键值id
            System.out.println(i);
            System.out.println("delete multiple successful!");
        }catch (Exception e){
            System.out.println(e);
        }

        //commit business
        sqlSession.commit();
        sqlSession.close();
    }
}
