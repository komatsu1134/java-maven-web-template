package com.xskj.utils;


import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.InputStream;

/**
 *
 * @author : xskj
 * @description: 封装的数据库连接的工具类 -- 连接池
 * @date : 2021/11/30 14:23
 */

public class SqlSessionServletFactory {


    private static  SqlSessionFactory sqlSessionFactory;
    
    //静态代码块： 在类被加载时调用，且只会调用一次
    static {
        try{
            //数据库连接
            String resource = "mybatis-config.xml";
            InputStream inputStream = Resources.getResourceAsStream(resource);
            sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //获取SqlSessionServletFactory
    public static SqlSessionFactory getSqlSessionFactory(){
        return  sqlSessionFactory;
    }
}
