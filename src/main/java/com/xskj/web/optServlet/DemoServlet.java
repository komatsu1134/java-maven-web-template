package com.xskj.web.optServlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author : xskj
 * @description: TODO
 * @date : 2021/11/30 23:41
 */


@WebServlet("/demo/*")
public class DemoServlet extends BaseServlet{

    /**
     * 接口方法：select  接口：  /demo/select
     */
    public void select(HttpServletRequest req, HttpServletResponse resp){
        System.out.println("-----------@@@@@@-------select----------");
    }


    /*
    * 接口方法：add   接口：/demo/add
    * */
    public void add(HttpServletRequest req, HttpServletResponse resp){
        System.out.println("-----------@@@@@@&&&&&&&---add---");
    }
}
