package com.xskj.mapper;

import com.xskj.pojo.Demo;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;

/**
 * @author : xskj
 * @description:  Demo实体类数据库操作接口
 * @date : 2021/11/30 18:32
 */
public interface DemoMapper {


    /*
     *  query all - 查询所有
     */
    List<Demo> selectAll();

    /*
     *  check detail by id - 通过id查看详情
     */
     Demo selectById(int id);

    /**
     * query condition - 条件查询
     * @param id
     * @param email
     * @param name
     * @return  List for Demo
     */
    //方法1
    //List<Demo> selectByCondition(@Param("status") int status,@Param("email") String email, @Param("name") String name );
    //方法2
    //List<Demo> selectByCondition(Demo demo);
    //方法3
    List<Demo> selectByCondition(Map map);

    /**
     * dynamic condition query - 动态条件查询
     * @param map
     * @return
     */
    List<Demo> selectDynamicByCondition(Map map);

    /**
     *
     * @return
     * @param map
     */
    List<Demo> selectSingleDynamicByCondition(Map map);

    /**
     * add - 添加
     * @param demo
     * @return
     */
    int add(Demo demo);



    /**
     * update - 更新
     * @param demo
     * @return
     */
    int update(Demo demo);

    /**
     * dynamic Update - 动态更新
     * @param demo
     * @return
     */
    int dynamicUpdate(Demo demo);

    /**
     * delete - 删除
     * @param demo
     * @return
     */
    int delete(Demo demo);

    /**
     * multiple delete - 批量删除
     * @param array
     * @return
     */
    int deleteMultiple(@Param("ids") int[] array);



}
