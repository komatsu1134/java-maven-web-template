package com.xskj.service;

/**
 * @author : xskj
 * @description: 这是demo服务类:这里以登录场景为例
 * @date : 2021/11/30 19:33
 */

public class DemoService {

    private String name = "xskj";
    private Integer passwd = 666666;


    /**
     * 用户登录验证
     * @param name
     * @param passwd
     * @return
     */
    public boolean test(String name, Integer passwd){
        if(name.equals(this.name) && passwd.equals(this.passwd)){
            return  true;
        }else{
            return false;
        }
    }

}
