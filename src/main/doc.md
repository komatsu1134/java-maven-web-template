# web project for java maven - java web 标准项目结构

## instruction 
* 采用MVC的架构模式
  ![img_2.png](img_2.png)
* 采用前后端分离的项目
  ![img_1.png](img_1.png)
* 采用三层架构
![img.png](img.png)

## plugin and dependency 
详情见pom.xml
* plugin: tomcat-maven-plugin
* dependency :mybatis servlet 

## project structure - 项目结构
![img_3.png](img_3.png)
## 需要配置好tomcat





