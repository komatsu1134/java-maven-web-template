# web project for java maven - java web 标准项目结构

## instruction 
* 采用MVC的架构模式
![输入图片说明](%E5%9B%BE%E7%89%87.png)
* 采用前后端分离的项目
![输入图片说明](12%E5%9B%BE%E7%89%87.png)
* 采用三层架构
![输入图片说明](122%E5%9B%BE%E7%89%87.png)

## plugin and dependency 
详情见pom.xml
* plugin: tomcat-maven-plugin
* dependency :mybatis servlet 

## project structure - 项目结构
![输入图片说明](%E6%96%87%E4%BB%B6%E7%9B%AE%E5%BD%95%E5%9B%BE%E7%89%87.png)
## 需要配置好tomcat





